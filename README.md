# kotlin-js-react-material

comming soon, if i find time ;)

## Description

Kotlin js React wrapper material components. The very most of them are based on the offical material components: https://material.io/develop/web . 
The official JS component implementation will be used as well. 

Only for the Date/Time picker https://flatpickr.js.org will be used and the data table is an complete own implementation.